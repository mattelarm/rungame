//
//  GameScene.swift
//  SpriteRunDemo
//
//  Created by IT-Högskolan on 2015-04-10.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    let playButton = SKSpriteNode(imageNamed: "level1")
    let level2Button = SKSpriteNode(imageNamed: "level2")
    

    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.playButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        self.addChild(self.playButton)
        self.level2Button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 60)
        self.addChild(self.level2Button)
        
        self.backgroundColor = UIColor.blackColor()
        
        var gameNameLabel = SKLabelNode(fontNamed: "Arial")
        gameNameLabel.text = "CoolGuy Jump Game"
        gameNameLabel.fontSize = 50
        gameNameLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 60)
        addChild(gameNameLabel)
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        for touch in touches {
            let location = touch.locationInNode(self)
            if self.nodeAtPoint(location) == self.playButton {
                var scene = PlayScene(size: self.size)
                let skView = self.view as SKView!
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene)
            }
            if self.nodeAtPoint(location) == self.level2Button {
                var scene = Level2Scene(size: self.size)
                let skView = self.view as SKView!
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene)
            }
        }
       
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
