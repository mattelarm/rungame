//
//  PlayScene.swift
//  SpriteRunDemo
//
//  Created by IT-Högskolan on 2015-04-10.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation



class PlayScene: SKScene, SKPhysicsContactDelegate {
    
    let hero = SKSpriteNode(imageNamed: "kevin")
    var onGround = false
    
    //Labels and Buttons
    let replayButton = SKSpriteNode(imageNamed: "tryAgain")
    var howToLabel = SKLabelNode(fontNamed: "Chalkduster")
    var finishLabel = SKLabelNode(fontNamed: "Chalkduster")
    let mainMenuButton = SKSpriteNode(imageNamed: "mainMenu")
    
    // Map
    var tileMap = JSTileMap(named: "myMap1.tmx")
    let coin = SKSpriteNode(imageNamed: "coin")
    
    //MoveButtons
    let jumpButton = SKSpriteNode(imageNamed: "jumpButton")
    let forwardButton = SKSpriteNode(imageNamed: "forwardButton")
    let backButton = SKSpriteNode(imageNamed: "backButton")
    
    //Move Map
    var xMovesPerSecond:CGFloat = 0
    var xMovesMapPerSecond:CGFloat = 0
    
    // CountDown
    var counterLabel = SKLabelNode(fontNamed: "Chalkduster")
    var counter = 53
    
    // superPower
    var superPowerBox = SKSpriteNode(imageNamed: "box")
    var superPower = 0
    
    // Musik
    var audioPlayer: AVAudioPlayer!
    var jumpSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("jump", ofType: "wav")!)
    var pickUpCoin = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("PickupCoin", ofType: "wav")!)
    var deathSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("death", ofType: "wav")!)
    var winSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("win", ofType: "wav")!)
    var powerUpSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("powerup", ofType: "wav")!)
    
    //kontakt typerna
    enum PhysicsType : UInt32 {
        case Hero   = 1
        case Ground  = 2
        case Spikes = 4
        case Coins = 8
        case SuperPoxerBox = 16
    }
    
    override func didMoveToView(view: SKView) {
        // Sätter gravity på hela världen.
        self.physicsWorld.gravity = CGVectorMake(0.0, -4)
       
        self.physicsWorld.contactDelegate = self
        
        // sätt backgrundfärgen
        self.backgroundColor = UIColor.blueColor()
        
        // tar bort ifall dom finns
        mainMenuButton.removeFromParent()
        replayButton.removeFromParent()
        finishLabel.removeFromParent()
        // ljudet
        audioPlayer = AVAudioPlayer(contentsOfURL: jumpSound, error: nil)
        audioPlayer.prepareToPlay()
        
        // sätt ut positionen på kartan i Scenen.
        self.anchorPoint = CGPoint(x: 0,y: 0)
        self.position = CGPoint(x: 0,y: 0)
        let point = tileMap.calculateAccumulatedFrame()
        println(point)
        tileMap.position = CGPoint(x:0,y:0)
        addChild(tileMap)
        
        // Lägger till golvet, gubben, tiden, text och pengar.
        addMoveButtons()
        addHowToText()
        addFloor()
        addHero()
        addCounter()
        addCoin()
        addSuperPowerBox()
        
    }
    
    func addFloor() {
        for var a = 0; a < Int(tileMap.mapSize.width); a++ { //Gå igenom varje punkt på kartan i witdh
            for var b = 0; b < Int(tileMap.mapSize.height); b++ { //Gå igenom varje punkt på kartan i höjd
                let layerInfo:TMXLayerInfo = tileMap.layers.firstObject as TMXLayerInfo //Get the first layer (you may want to pick another layer if you don't want to use the first one on the tile map)
                
                let point = CGPoint(x: a, y: b)
                let gid = layerInfo.layer.tileGidAt(layerInfo.layer.pointForCoord(point)) //Id på tilesetet som är inladdat i myMap. Alla dom har en id som startar på 0.
                println(gid)
                println(point)
                if gid == 142 || gid == 112 || gid == 111 || gid == 95 || gid == 96 || gid == 127 { //Kollar om id på golvet som ligger i tileSetet och säger att det är dom med det id som ska ha pysics på.
                    let node = layerInfo.layer.tileAtCoord(point) //
                    node.physicsBody = SKPhysicsBody(rectangleOfSize: node.frame.size) //sätt en physivsBody på marken
                    node.physicsBody?.dynamic = false
                    
                    node.physicsBody?.usesPreciseCollisionDetection = true
                    node.physicsBody!.categoryBitMask = PhysicsType.Ground.rawValue
                    
                }
                if gid == 144{
                    let spike = layerInfo.layer.tileAtCoord(point)
                    spike.physicsBody = SKPhysicsBody(texture: spike.texture, size: spike.size)
                    spike.physicsBody?.dynamic = false
                    spike.physicsBody?.usesPreciseCollisionDetection = true
                    spike.physicsBody?.categoryBitMask = PhysicsType.Spikes.rawValue
                    
               }
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        //Kallas när två objekt kommer i kontakt med varandra.
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch(contactMask) {
            
        case PhysicsType.Hero.rawValue | PhysicsType.Spikes.rawValue:
            onGround = false
            hero.removeFromParent()
            gameOver()
            
        case PhysicsType.Hero.rawValue | PhysicsType.Ground.rawValue:
            onGround = true
            
        case PhysicsType.Hero.rawValue | PhysicsType.Coins.rawValue:
            audioPlayer = AVAudioPlayer(contentsOfURL: pickUpCoin, error: nil)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            coin.removeFromParent()
            
        case PhysicsType.Hero.rawValue | PhysicsType.SuperPoxerBox.rawValue:
            superPowerBox.removeFromParent()
            audioPlayer = AVAudioPlayer(contentsOfURL: powerUpSound, error: nil)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            superPower++
            
        default:
            return
            
        }
        
    }
    
    func didEndContact(contact: SKPhysicsContact) {
        
        // Kallas när två objekt tappar kontakt med varandra.
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch(contactMask) {
            
        case PhysicsType.Hero.rawValue | PhysicsType.Spikes.rawValue:
            onGround = false
            hero.removeFromParent()
            
//        case PhysicsType.Hero.rawValue | PhysicsType.Ground.rawValue:
//            onGround = true
//        
//        case PhysicsType.Hero.rawValue | PhysicsType.Coins.rawValue:
//            coin.removeFromParent()
//        
//        case PhysicsType.Hero.rawValue | PhysicsType.SuperPoxerBox.rawValue:
//            superPowerBox.removeFromParent()
            
        default:
            return
            
        }
        
    }
    
    func addHowToText() {
        howToLabel.text = "Take CoolGuy to the finish before times run out!"
        howToLabel.fontSize = 30
        howToLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 50)
        howToLabel.zPosition = 20
        
        howToLabel.runAction(SKAction.fadeOutWithDuration(7.0))
        addChild(howToLabel)
    }
    
    func addFinishLabel() {
        finishLabel.text = "Just jump of Screen to Complete Level"
        finishLabel.fontSize = 25
        finishLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 80)
        finishLabel.zPosition = 20
        
        finishLabel.runAction(SKAction.fadeInWithDuration(8.0))
        addChild(finishLabel)
    }
    
    func addCounter() {
        counterLabel.name = "myCounterLabel"
        counterLabel.text = "Time left: 54"
        counterLabel.fontSize = 40
        counterLabel.fontColor = SKColor.yellowColor()
        counterLabel.zPosition = 200
        counterLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height - 40)
        
        addChild(counterLabel)
    }
    
    
    
    func gameOver() {
        replayButton.removeFromParent()
        hero.removeFromParent()
        mainMenuButton.removeFromParent()
        
        counterLabel.removeAllActions()
        if(!audioPlayer.playing) {
        audioPlayer = AVAudioPlayer(contentsOfURL: deathSound, error: nil)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        }
        
        var gameOverLabel = SKLabelNode(fontNamed: "Arial")
        gameOverLabel.text = "Game over Man"
        gameOverLabel.fontSize = 70
        gameOverLabel.zPosition = 30
        gameOverLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        addChild(gameOverLabel)
        
        mainMenuButton.position = CGPointMake(CGRectGetMidX(self.frame) + 80, CGRectGetMidY(self.frame) - 50)
        mainMenuButton.name = "mainMenuButton"
        mainMenuButton.zPosition = 50
        addChild(mainMenuButton)
        
        replayButton.position = CGPointMake(CGRectGetMidX(self.frame) - 80, CGRectGetMidY(self.frame) - 50)
        replayButton.name = "replayButton"
        replayButton.zPosition = 50
        addChild(replayButton)
        
        
        xMovesMapPerSecond = 0
        
    }
    func addMoveButtons() {
        jumpButton.position = CGPointMake(CGRectGetMaxX(self.frame) - jumpButton.size.width, CGRectGetMinY(self.frame) + jumpButton.size.height / 2)
        jumpButton.name = "jumpButton"
        
        forwardButton.position = CGPointMake(CGRectGetMaxX(self.frame) - forwardButton.size.width - backButton.size.width, CGRectGetMinY(self.frame) + forwardButton.size.height / 2)
        forwardButton.name = "forwardButton"
        
        backButton.position = CGPointMake(CGRectGetMinX(self.frame) + backButton.size.width / 2, CGRectGetMinY(self.frame) + backButton.size.height / 2)
        backButton.name = "backButton"
        
        backButton.zPosition = 800
        forwardButton.zPosition = 800
        jumpButton.zPosition = 800
        
        
        addChild(jumpButton)
        addChild(forwardButton)
        addChild(backButton)

    
    }
    func wonGame() {
        replayButton.removeFromParent()
        hero.removeFromParent()
        mainMenuButton.removeFromParent()
        
        counterLabel.removeAllActions()
        audioPlayer = AVAudioPlayer(contentsOfURL: winSound, error: nil)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        audioPlayer.numberOfLoops = 1
        
        var winningLabel = SKLabelNode(fontNamed: "Chalkduster")
        winningLabel.text = "Congratz Man! You are Awsome"
        winningLabel.fontSize = 30
        winningLabel.fontColor = SKColor.yellowColor()
        winningLabel.zPosition = 70
        winningLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        addChild(winningLabel)
        
        mainMenuButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 50)
        mainMenuButton.name = "mainMenuButton"
        mainMenuButton.zPosition = 50
        addChild(mainMenuButton)
        
        xMovesMapPerSecond = 0
        
    }
    
    func addCoin() {
        coin.physicsBody?.dynamic = false
        
        coin.physicsBody = SKPhysicsBody(texture: coin.texture, size: coin.size)
        coin.physicsBody?.categoryBitMask = PhysicsType.Coins.rawValue
        coin.physicsBody?.usesPreciseCollisionDetection = true
        coin.position = CGPoint(x: tileMap.position.x + 2000, y: 100)
        coin.physicsBody?.friction = 0.0
        coin.physicsBody?.restitution = 0.0
        coin.physicsBody?.linearDamping = 0.0
        coin.physicsBody?.allowsRotation = false
        tileMap.addChild(coin)
    }
    
    func addSuperPowerBox(){
        superPowerBox.physicsBody?.dynamic = false
        superPowerBox.physicsBody = SKPhysicsBody(rectangleOfSize: superPowerBox.size)
        superPowerBox.physicsBody?.categoryBitMask = PhysicsType.SuperPoxerBox.rawValue
        superPowerBox.physicsBody?.usesPreciseCollisionDetection = true
        superPowerBox.position = CGPoint(x: tileMap.position.x + 900, y: 80)
        superPowerBox.physicsBody?.friction = 0.0
        superPowerBox.physicsBody?.restitution = 0.0
        superPowerBox.physicsBody?.linearDamping = 0.0
        superPowerBox.physicsBody?.allowsRotation = false
        tileMap.addChild(superPowerBox)
    }


    func addHero(){
     
        setHeroStartPosition()
        hero.zPosition = 500
        hero.physicsBody = SKPhysicsBody(texture: hero.texture, size: hero.size)
        
        hero.physicsBody?.dynamic = true
        hero.physicsBody?.restitution = 0
        hero.physicsBody?.allowsRotation = false
        hero.physicsBody?.density = 3.0
        hero.physicsBody?.affectedByGravity = true
        hero.physicsBody?.linearDamping = 1.0
        
        hero.physicsBody?.categoryBitMask = PhysicsType.Hero.rawValue
        hero.physicsBody?.collisionBitMask = PhysicsType.Ground.rawValue | PhysicsType.Spikes.rawValue | PhysicsType.Coins.rawValue | PhysicsType.SuperPoxerBox.rawValue
        hero.physicsBody?.contactTestBitMask = PhysicsType.Ground.rawValue | PhysicsType.Spikes.rawValue | PhysicsType.Coins.rawValue | PhysicsType.SuperPoxerBox.rawValue
        
        hero.physicsBody?.friction = 1.0
        
        addChild(hero)
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.allObjects.first as UITouch
        //kollar vart man klickar på skärmen
        let location = touch.locationInNode(self)
        
        // Kollar om man klickar på knappen replayButton som kommer upp när man dör.
        // Då startar banan om eller meny knappen.
        if nodeAtPoint(location) == replayButton{
            var scene = PlayScene(size: self.size)
            let skView = self.view as SKView!
            skView.ignoresSiblingOrder = true
            scene.size = skView.bounds.size
            scene.scaleMode = .ResizeFill
            skView.presentScene(scene)
        }
        
        if nodeAtPoint(location) == mainMenuButton{
            var scene = GameScene(size: self.size)
            let skView = self.view as SKView!
            skView.ignoresSiblingOrder = true
            scene.size = skView.bounds.size
            scene.scaleMode = .ResizeFill
            skView.presentScene(scene)
        }
        
        // kolla vilken sida användaren trycker på. och gå fram eller bakåt beroende på vart.
        if nodeAtPoint(location) == backButton {
            // flyttar gubben bakåt på skärmen
            
            xMovesPerSecond = -2
            println(xMovesPerSecond)
            if (hero.position.x > self.frame.size.width/2){
                hero.position.x = self.frame.size.width/2
                xMovesMapPerSecond = 5
                xMovesPerSecond = -2
            }
        }
            // xMovesMapPerSecond = -3
            if nodeAtPoint(location) == forwardButton {
                // flyttar gubben framåt på skärmen
                
                xMovesPerSecond = 2
                xMovesMapPerSecond = -5
                
                var wait = SKAction.waitForDuration(1.0)
                var run = SKAction.runBlock {
                    self.counterLabel.text = ("Time left: \(self.counter)")
                    self.counter--
                }
                
                if(!counterLabel.hasActions()) {
                    counterLabel.runAction(SKAction.repeatActionForever(SKAction.sequence([wait, run])))
                }
                
            }
           
           
            if nodeAtPoint(location) == jumpButton {
                if (onGround == true) {
                    onGround = false
                    audioPlayer.play()
                    hero.physicsBody?.velocity.dy = 320
                    hero.physicsBody?.usesPreciseCollisionDetection = true
            }
           
            
        }
        
        
    }
        
//        
//        
//        if nodeAtPoint(location) == backButton {
//            if location.x > self.frame.size.width / 2 {
//                // flyttar gubben framåt på skärmen
//                
//                xMovesPerSecond = 2
//                xMovesMapPerSecond = -5
//                
//                var wait = SKAction.waitForDuration(1.0)
//                var run = SKAction.runBlock {
//                    self.counterLabel.text = ("Time left: \(self.counter)")
//                    self.counter--
//                }
//                
//                if(!counterLabel.hasActions()) {
//                counterLabel.runAction(SKAction.repeatActionForever(SKAction.sequence([wait, run])))
//                }
//                
//                
//            } else {
//                // flyttar gubben bakåt på skärmen
//                
//                xMovesPerSecond = -2
//                println(xMovesPerSecond)
//                if (hero.position.x > self.frame.size.width/2){
//                    hero.position.x = self.frame.size.width/2
//                    xMovesMapPerSecond = 5
//                    xMovesPerSecond = -2
//                }
//               // xMovesMapPerSecond = -3
//                
//               
//            }
//        } else {
//            if (onGround == true) {
//                onGround = false
//                
//            hero.physicsBody?.velocity.dy = 320
//            hero.physicsBody?.usesPreciseCollisionDetection = true
//            }
//            
//        }
//        
//        
//    }
//    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        xMovesPerSecond = 0
        xMovesMapPerSecond = 0
        hero.physicsBody?.velocity.dx = 0
    }
    
    func setHeroStartPosition() {
        hero.position = CGPointMake(200, 150)
    }
    
    override func update(currentTime: NSTimeInterval) {
        
        // kolla om gubben har åkt ner ifrån skärmen isf ta bort honom ifrån skärmen och skriv ut att han dog
        if(hero.position.y < 0) {
            gameOver()
        }
        
        if ( counter < 0) {
            gameOver()
        }
        
        if(superPower > 0){
            self.physicsWorld.gravity = CGVectorMake(0.0, -2.0)
            if(tileMap.position.x == -1300){
                superPower--
                self.physicsWorld.gravity = CGVectorMake(0.0, -4)
            }
        }
        
        // Kolla om gubben är i luften och sätt att han inte är på marken till false så han inte kan dubbelhoppa.
        if (hero.physicsBody?.velocity.dy > 150) {
            onGround = false
        }
        // Gubben hoppar ut ifrån banan och då vinner man
        if(tileMap.position.x == -2300) {
            finishLabel.removeFromParent()
            addFinishLabel()
        }
        // Klara spelet när gubben hoppar ut ifrån banan
        if(tileMap.position.x == -2860) {
            finishLabel.removeFromParent()
            if(hero.position.x > 330) {
                wonGame()
            }
        }
        
       // tileMap.position.x = -hero.position.x
        
        tileMap.position.x += xMovesMapPerSecond
        
        if (hero.position.x <= self.frame.size.width/2 ) {
            
            if xMovesPerSecond != 0 {
                hero.position.x += xMovesPerSecond
            }
        }
        
        
    }
    

    
}